# Olukyukudar's dotfiles

## Screenshot
| ![Alt text](./screenshot/Desktop1.png?raw=true) |![Alt text](./screenshot/Desktop2.png?raw=true) |
| --- | --- |

## Personal Setup
The setup I use in laptop:
* **Distrio**: [Arch Linux](https://www.archlinux.org/)
* **Window Manager**: [Dwm](https://dwm.suckless.org/)
* **Terminal**: Alacritty
* **Shell**: ZSH with [Oh My Zsh](https://ohmyz.sh/)
* **Editor**: Vim
* **Theme**: [Flat-Remix-GTK](https://github.com/daniruiz/Flat-Remix-GTK)
* **Browser**: Firefox
* **File Browser**: Ranger (term), Nemo (GUI)

Startup applications are controlled by `.xinitrc`. If you use any other way, you have to modify the configs.


### Basic Keyboard Shortcuts
* `Super + Enter`: Spawn a terminal
* `Super + R`: Launch Rofi
* `Super + W`: Close focused client
* `Super + Arrow Keys`: Change focus by direction
* `Super + Shift + Space`: Make focused client master
* `Super + Numbers`: Move to a tag
* `Super + Shift + Numbers`: Move focused client to a tag
* `Super + E`: Emoji menu
* `Super + Alt + Q`: Restart Dwm
* `Super + L`: Lock screen
* `Super + Alt + W`: Launch Firefox
* `Super + Alt + Enter`: Launch file manager (Nemo)
* `Super + Shift + Enter`: Launch file manager (Ranger)

